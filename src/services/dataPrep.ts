import { sortedIndex, sortedLastIndex } from "lodash";

const NUM_HISTORIC_YEARS =
  CONST.LAST_HISTORIC_YEAR - CONST.FIRST_HISTORIC_YEAR + 1;

const INITIAL_STATE = {
  loadedCurrent: false,
  loadedHistoric: false,
  current: null,
  historic: [],
  clinchedPPGs: [],
  missedPPGs: [],
  error: null,
};

// Takes in a ppg val, and sorted asc arrays: historical clinched ppgs and missed ppgs
// Returns 0-100 (2 decimals) historical chances of clinching
export function clinchOddsByPPG(ppg, clinched, missed) {
  let clinchesAtOrBelow = sortedLastIndex(clinched, ppg);
  if (ppg === -1) return -1;
  let missesAtOrAbove = missed.length - sortedIndex(missed, ppg);
  return (
    (clinchesAtOrBelow / (clinchesAtOrBelow + missesAtOrAbove)) *
    100
  ).toFixed(0);
}

import teamsParser from "./teamsParser";

export function getTeamsByPPG(data) {
  let teams = [];

  data.records[0].teamRecords.forEach((record) => {
    let ppg = (record.points / record.gamesPlayed).toFixed(3);

    let team = teamsParser.getTeamByID(record.team.id);
    team["gp"] = record.gamesPlayed;
    team["ppg"] = isNaN(ppg) ? -1 : ppg;
    teams.push(team);
  });

  // sort by gp descending
  teams.sort((a, b) => {
    return b.gp - a.gp;
  });
  // sort by ppg descending
  teams.sort((a, b) => {
    return b.ppg - a.ppg;
  });
  return teams;
}

export function getPPGByClinch(data) {
  let clinched = [];
  let missed = [];

  data.forEach((year) => {
    year.records[0].teamRecords.forEach((record) => {
      let ppg = (record.points / record.gamesPlayed).toFixed(4);
      let didClinch = record.clinchIndicator;
      if (didClinch) clinched.push(ppg);
      else missed.push(ppg);
    });
  });

  return {
    clinched: clinched.sort(),
    missed: missed.sort(),
  };
}

// ----------------

import teams from "../../constants/teams";

function getTeamByID(id) {
  return teams.find((team) => id === team.id);
}

export default { getTeamByID };
